//
//  UserDataCrawler.swift
//  FBCrawler
//
//  Created by HereTrix on 7/25/16.
//  Copyright © 2016 HereTrix. All rights reserved.
//

import UIKit

import Fuzi

import WebKit

// MARK: - Constants
private let fbBaseUrl = "https://www.facebook.com/"
private let twitterUrl = "https://www.twitter.com/"
private let fbLoginUrl = "https://www.facebook.com/login.php?login_attempt=1"
private let fbAboutPage = "/about"
private let fbLiveEventsPage = "/about?section=year-overviews"
private let fbWorkAndEducationPage = "/about?section=education"
private let fbLivePlacesPage = "/about?section=living"

protocol UserDataCrawlerDelegate {
    func dataCrawlerDidFinishProcessing()
    func dataCrawlerFailedProcessing(error: NSError)
}

let CrawlerErrorDomain = "DataCrawler"

// MARK: - Data Crawler
class UserDataCrawler: NSObject, WKNavigationDelegate, WKScriptMessageHandler {

    var delegate: UserDataCrawlerDelegate?
    
    var isReady: Bool = false
    
    private var isFacebook:Bool?
    private var containerView: UIView!
    private var webView: WKWebView!
    private var parserQueue: dispatch_queue_t = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    private var loadingLock: dispatch_semaphore_t!
    
    private var dataContainer: [String:AnyObject] = [:]
    
    required init(container: UIView) {
        super.init()
        clearCookies()
        self.containerView = container
        
        let contentController = WKUserContentController();
        contentController.addScriptMessageHandler(self, name: "callbackHandler")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        webView = WKWebView(frame: containerView.bounds, configuration: config)
        containerView.addSubview(webView)
        webView.hidden = true
        self.webView.navigationDelegate = self
        webView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0"
    }
    
    deinit {
        dispatch_semaphore_signal(self.loadingLock)
        webView.removeFromSuperview()
    }
    
    func isFacebookValue(facebook: Bool) {
        
        isFacebook = facebook
    }
    
    func report() -> String {
        let data = try! NSJSONSerialization.dataWithJSONObject(dataContainer, options: [])
        let report = String(data: data, encoding: NSUTF8StringEncoding)!
        return report
    }
    
    // MARK: - Web view processing
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        
        guard let path = webView.URL?.path else {
            let errorMessage = "Failed to process URL"
            let error = NSError(domain: CrawlerErrorDomain,
                                code: 0,
                                userInfo: [NSLocalizedDescriptionKey : errorMessage])
            dispatch_semaphore_signal(self.loadingLock)
            delegate?.dataCrawlerFailedProcessing(error)
            return
        }
        
        debugPrint(path)
        
        if path.containsString("login") {
            // lock until login finished
            return
        }
        dispatch_semaphore_signal(loadingLock)
        
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        
        print("\(message.name):  \(message.body)")
        
        if let data = message.body as? NSDictionary {
            
            if let errorMessage = data["error"] {
                
                if isFacebook == false {
                    
                    if webView.URL?.absoluteString != twitterUrl {
                        let error = NSError(domain: CrawlerErrorDomain,
                                            code: 0,
                                            userInfo: [NSLocalizedDescriptionKey : errorMessage])
                        dispatch_semaphore_signal(loadingLock)
                        delegate?.dataCrawlerFailedProcessing(error)
                    }

                } else {
                
                    if webView.URL?.absoluteString != fbBaseUrl {
                        let error = NSError(domain: CrawlerErrorDomain,
                                            code: 0,
                                            userInfo: [NSLocalizedDescriptionKey : errorMessage])
                        dispatch_semaphore_signal(loadingLock)
                        delegate?.dataCrawlerFailedProcessing(error)
                    }
                }
                return
            }
            
            for (key, value) in data {
                dataContainer[key as! String] = value
            }
        }
    }
    
    // MARK: - Data methods
    func loginUser(usermail: String, userpass: String, completion: () -> Void) {
        
        var javaScriptString = ""
        
        if isFacebook == true {
            
            javaScriptString = "var emailField = document.forms[0].elements['email'];" +
                "if (typeof emailField !== 'undefined') {" +
                "emailField.value = '\(usermail)';" +
                "document.forms[0].elements['pass'].value = '\(userpass)';" +
                "document.forms[0].submit();" +
                "} else {" +
            "webkit.messageHandlers.callbackHandler.postMessage({'error' : 'Login is not valid'});}"
            
        } else {
            
            javaScriptString = "var emailField = document.forms[2].elements[0];" + "if (typeof emailField !== 'undefined') {" + "emailField.value = '\(usermail)';" + "document.forms[2].elements[1].value = '\(userpass)';" + "document.forms[2].submit();" + "} else {" + "webkit.messageHandlers.callbackHandler.postMessage({'error' : 'Login is not valid'});}"
        }
        
        let userScript = WKUserScript(
            source: javaScriptString,
            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
            forMainFrameOnly: false
        )
        
        if isFacebook == false {
            
            loadUrlString(twitterUrl, userScript: userScript, completion: {
                completion()
            })
            
        } else {
            
            loadUrlString(fbLoginUrl, userScript: userScript, completion: {
                completion()
            })
        }
    }
    
    func getUserData(userid: String) {
        
        dataContainer = [:]
        
        var user = ""
        
        if self.isFacebook == true {
            
            user = userid.stringByReplacingOccurrencesOfString(fbBaseUrl, withString: "")
            
        } else {
            
            user = userid.stringByReplacingOccurrencesOfString(twitterUrl, withString: "")
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { [weak self] in
            
            print("Fetching STARTED \(NSDate())")
            if self!.isFacebook == true {
            self?.getProfilePhoto(user)
            self?.getBirthDate(user)
            self?.getWorkAndEducation(user)
            self?.getLivingPlaces(user)
                
            } else {
                
                self?.getBirthDate(user)
                self?.getProfilePhoto(user)
            }
            
            self?.delegate?.dataCrawlerDidFinishProcessing()
            
            print("Fetching FINISHED \(NSDate())")
        })
    }
    
    private func getBirthDate(userId: String) {
        
        print("fetching date of birth")
        
        if self.isFacebook == true {
        
        if let userScript = scriptFromFile("getBirthDate") {
            
            let page = "\(fbBaseUrl)\(userId)\(fbLiveEventsPage)"
            executeScript(userScript, page: page)
            
        } else {
            print("Can't execute Birthdate script")
        }
        } else {
            
            if let userScript = scriptFromFile("birthdate") {
                
                let page = "\(twitterUrl)\(userId)"
                executeScript(userScript, page: page)
                
            } else {
                print("Can't execute Birthdate script")
            }

        }
    }
    
    private func getWorkAndEducation(userId: String) {
        print("fetching work")
        if let userScript = scriptFromFile("getWorkAndEducation") {
            
            let page = "\(fbBaseUrl)\(userId)\(fbWorkAndEducationPage)"
            executeScript(userScript, page: page)
        } else {
            print("Can't execute WorkAndEducation script")
        }
    }
    
    private func getProfilePhoto(userid: String) {
        
        print("Fetching photo")
        
        if self.isFacebook == true {
    
        if let userScript = scriptFromFile("getAvatarUrl") {
            
            let page = "\(fbBaseUrl)\(userid)\(fbAboutPage)"
            executeScript(userScript, page: page)
        } else {
            print("Can't execute Avatar script")
        }
        } else {
            
            if let userScript = scriptFromFile("avatar") {
                
                let page = "\(twitterUrl)\(userid)"
                executeScript(userScript, page: page)
                
            } else {
               print("Can't execute Avatar script")
            }

        }
    }
    
    private func getLivingPlaces(userId: String) {
        print("fetching living places")
        if let userScript = scriptFromFile("getLivingPlaces") {
            
            let page = "\(fbBaseUrl)\(userId)\(fbLivePlacesPage)"
            executeScript(userScript, page: page)
        } else {
            print("Can't execute Living Places script")
        }
    }
    
    // MARK: - Utility
    private func loadUrlString(urlString: String,
                               userScript: WKUserScript,
                               completion: () -> Void) {
        
        dispatch_async(parserQueue) {
            // Create lock
            self.loadingLock = dispatch_semaphore_create(0)
            
            // Preform request on main queue
            dispatch_async(dispatch_get_main_queue(), {
                
                let contentController = self.webView.configuration.userContentController
                
                contentController.removeAllUserScripts()
                self.webView.configuration.userContentController.addUserScript(userScript)
                
                let url = NSURL(string: urlString)
                let request = NSURLRequest(URL: url!)
                self.webView.loadRequest(request)
            })
            
            dispatch_semaphore_wait(self.loadingLock, DISPATCH_TIME_FOREVER)
            
            completion()
        }
    }
    
    private func executeScript(userScript: WKUserScript, page: String) {
        let semaphore = dispatch_semaphore_create(0)
        loadUrlString(page, userScript: userScript) {
            dispatch_semaphore_signal(semaphore)
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    }
    
    private func scriptFromFile(filename: String) -> WKUserScript? {
        
        if let path = NSBundle.mainBundle().pathForResource(filename, ofType: "js") {
        
            do {
                let javaScriptString = try String(contentsOfFile: path)
                let userScript = WKUserScript(
                    source: javaScriptString,
                    injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
                    forMainFrameOnly: true
                )
                return userScript
            } catch {
                
            }
        }
        
        return nil
    }
    
    private func clearCookies() {
        
        let libraryPath = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true).first
        if let cookiesPath = libraryPath?.stringByAppendingString("/Cookies") {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(cookiesPath)
            } catch {
                debugPrint("Can't clear cookies")
            }
        }
    }
}
