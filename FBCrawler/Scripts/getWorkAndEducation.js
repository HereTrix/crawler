var work_item = document.querySelectorAll('[data-pnref="work"]')[0];
if (typeof work_item !== 'undefined') {
    var work_list = work_item.getElementsByTagName('ul')[0].getElementsByTagName('li');
    var places = [];
    for (var i = 0; i < work_list.length; i++) {
        var item = work_list[i];
        places.push(item.textContent);
    }
    webkit.messageHandlers.callbackHandler.postMessage({'work' : places});
}

var education_item = document.querySelectorAll('[data-pnref="edu"]')[0];
var education_list = education_item.getElementsByTagName('ul')[0].getElementsByTagName('li');
var places = [];
for (var i = 0; i < education_list.length; i++) {
    var item = education_list[i];
    places.push(item.textContent);
}
webkit.messageHandlers.callbackHandler.postMessage({'education' : places});