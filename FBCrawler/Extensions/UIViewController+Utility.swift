//
//  UIViewController.swift
//  FBCrawler
//
//  Created by HereTrix on 7/26/16.
//  Copyright © 2016 HereTrix. All rights reserved.
//

import UIKit

extension UIViewController {

    // MARK: - Utility
    func showMessage(message: String) {
        let ac = UIAlertController(title: nil,
                                   message: message,
                                   preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK",
                                   style: .Default, handler: nil)
        ac.addAction(action)
        self.presentViewController(ac, animated: true, completion: nil)
    }
}
