//
//  ViewController.swift
//  FBCrawler
//
//  Created by HereTrix on 7/25/16.
//  Copyright © 2016 HereTrix. All rights reserved.
//

import UIKit

import WebKit
import Fuzi

private let usermailFacebook = "crbqwe@gmail.com"
private let userpassFacebook = "crb12345"

private let usermailTwitter = "380936655133"
private let userpassTwitter = "velikolepniy34"

private let LoginSegue = "LoginSegue"

class CrawlerLoginViewController: UIViewController {
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    
    var isFacebook: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        credentials()
        
        self.navigationItem.rightBarButtonItem = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func credentials() {
        
        if isFacebook == true {
            
            emailField.text = usermailFacebook
            passwordField.text = userpassFacebook
            
        } else {
            
            emailField.text = usermailTwitter
            passwordField.text = userpassTwitter
        }

    }
    
    // MARK: - Actions
    @IBAction func processUser() {
        
        if let email = emailField.text,
            let pass = passwordField.text
            where email.characters.count > 0 &&
                pass.characters.count > 0 {
            
            performSegueWithIdentifier(LoginSegue, sender: self)
        } else {
            showMessage("Login and Password shouldn't be empty")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == LoginSegue {
            let dest = segue.destinationViewController as! CrawlerViewController
            dest.usermail = emailField.text
            dest.userpass = passwordField.text
            dest.isFacebook = isFacebook
        }
    }
}

