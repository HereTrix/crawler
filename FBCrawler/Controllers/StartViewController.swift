import UIKit

private let StartSegue = "StartSegue"

class StartViewController: UIViewController {

    @IBOutlet var twitterBtn: UIButton!
    @IBOutlet var facebookBtn: UIButton!
    
    var isFacebook: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func twitterAction() {
        
        isFacebook = false
        
        performSegueWithIdentifier(StartSegue, sender: nil)
    }
    
    @IBAction func facebookAction() {
        
        isFacebook = true
        
        performSegueWithIdentifier(StartSegue, sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == StartSegue {
            let dest = segue.destinationViewController as! CrawlerLoginViewController
            dest.isFacebook = isFacebook
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
