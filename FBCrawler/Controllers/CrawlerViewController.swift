//
//  CrawlerViewController.swift
//  FBCrawler
//
//  Created by HereTrix on 7/26/16.
//  Copyright © 2016 HereTrix. All rights reserved.
//

import UIKit

import PKHUD

class CrawlerViewController: UIViewController, UserDataCrawlerDelegate {

    var usermail: String!
    var userpass: String!
    
    @IBOutlet var reportItem: UIBarButtonItem!
    @IBOutlet var containerView: UIView!
    @IBOutlet var userIdField: UITextField!
    
    private var dataCrawler: UserDataCrawler!
    
    var isFacebook: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem?.title = "Logout"
        navigationItem.rightBarButtonItem = nil
        dataCrawler = UserDataCrawler(container: containerView)
        dataCrawler.isFacebookValue(isFacebook!)
        dataCrawler.delegate = self
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        dataCrawler.loginUser(usermail, userpass: userpass) {
            PKHUD.sharedHUD.hide(animated: true, completion: nil)
        }
    }
    
    private func processUser(userId: String) {
        dataCrawler.getUserData(userId)
    }
    
    @IBAction func processAction() {
        if let userId = userIdField.text where userId.characters.count > 0 {
            PKHUD.sharedHUD.contentView = PKHUDProgressView()
            PKHUD.sharedHUD.show()
            processUser(userId)
        } else {
            showMessage("Please, enter user id")
        }
    }
    
    @IBAction func showReportAction() {
        showMessage(dataCrawler.report())
    }
    // MARK: - Data Crawler delegate
    func dataCrawlerDidFinishProcessing() {
        dispatch_async(dispatch_get_main_queue()) { 
            PKHUD.sharedHUD.hide(animated: true, completion: nil)
            self.showMessage("Data processing finished")
            self.navigationItem.rightBarButtonItem = self.reportItem
        }
    }
    
    func dataCrawlerFailedProcessing(error: NSError) {
        showMessage(error.localizedDescription)
        PKHUD.sharedHUD.hide(animated: true, completion: nil)
    }
}
